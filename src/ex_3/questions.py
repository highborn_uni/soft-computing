import sys
import traceback
from functools import wraps
from time import sleep

import numpy as np
from tensorflow_core.python.keras.layers import Conv2D, Activation, MaxPooling2D, Dropout, Dense, Flatten, LeakyReLU, \
    BatchNormalization

from src.ex_2.auto_encoder_classifier import AutoEncoderClassifier
from src.ex_2.questions import show_sample
from src.ex_3.auto_encoder_keras import AutoEncoderKeras
from src.ex_3.cifar10 import CifarKerasCNN
from src.ex_3.cifar10_mobilenet import CifarMobileNetCNN


def q_2():
    dir_name = 'AutoEncoderKeras-20191220173039'
    ann = AutoEncoderKeras(activation=0, hidden_layers=[128], alpha=0.001, batch_size=16)
    ann.fit('train_6.csv')
    classifier = AutoEncoderClassifier(ann, dir_name)
    data, label = read_train_data()
    classifier.fit(data, label)
    data, data_6, data_non_6, label = read_test_data(data, label)
    predict_array = classifier.classify(data, crisp=True)
    classifier.make_result(label, predict_array, verbose=True)
    raw_predict_array = classifier.classify(data)
    raw_positive = raw_predict_array[:data_6.shape[0]]
    raw_negative = raw_predict_array[data_6.shape[0]:]
    for j in range(1, 4):
        idx = raw_positive.argsort()[-j]
        print(predict_array[idx], raw_positive[idx])
        sample = data_6[idx]
        show_sample(sample)
    for j in range(0, 3):
        idx = raw_negative.argsort()[j]
        print(predict_array[data_6.shape[0] + idx], raw_negative[idx])
        sample = data_non_6[idx]
        show_sample(sample)


def read_test_data(data, label):
    data_6, _ = AutoEncoderKeras.read_data('test_6.csv')
    data_non_6, _ = AutoEncoderKeras.read_data('test_non_6.csv')
    data = np.concatenate((data_6, data_non_6), axis=0)
    label = np.zeros((data.shape[0],))
    label[data_6.shape[0]:] = 1
    return data, data_6, data_non_6, label


def read_train_data():
    data_6, _ = AutoEncoderKeras.read_data('train_6.csv')
    data_non_6, _ = AutoEncoderKeras.read_data('train_non_6.csv')
    data = np.concatenate((data_6, data_non_6), axis=0)
    label = np.zeros((data.shape[0],))
    label[data_6.shape[0]:] = 1
    return data, label


DEBUG_MODE = True


def q_wrapper(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        try:
            x_train, y_train, x_test, y_test = CifarKerasCNN.read_cifar_data()
            learning_rate = 0.01
            layers_list = [
                Conv2D(7, (3, 3), padding='same', input_shape=x_train.shape[1:]),
                Activation('relu'),
                Conv2D(9, (3, 3)),
                Activation('relu'),
                MaxPooling2D(pool_size=(2, 2)),
                Dropout(0.25),
                Flatten(),
                Dense(10),
                Activation('softmax'),
            ]
            optimizer = 'Adam'
            loss_function = 'categorical_crossentropy'
            batch_size = 32
            x_train, learning_rate, layers_list, optimizer, loss_function, batch_size = f(
                x_train,
                learning_rate,
                layers_list,
                optimizer,
                loss_function,
                batch_size,
            )
            manager = CifarKerasCNN(
                learning_rate=learning_rate,
                layers_list=layers_list,
                optimizer=optimizer,
                loss_function=loss_function,
                batch_size=batch_size,
            )
            out_dir = manager.fit(x_train, y_train)
            manager.test(x_test, y_test, out_dir=out_dir)
        except Exception as e:
            if DEBUG_MODE is True:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback,
                                          limit=2, file=sys.stdout)
            else:
                raise e

    return wrap


@q_wrapper
def q_3_0(*args):
    return args


@q_wrapper
def q_3_1_1(*args):
    idx = 2
    layers_list = args[idx]
    layers_list.insert(2, Activation('relu'))
    layers_list.insert(2, Conv2D(7, (3, 3)))
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_1_2(*args):
    idx = 2
    layers_list = args[idx]
    for i in range(2):
        layers_list.insert(2, Activation('relu'))
        layers_list.insert(2, Conv2D(7, (3, 3)))
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_2_1(*args):
    idx = 2
    x_train = args[0]
    layers_list = args[idx]
    layers_list[0] = Conv2D(7, (5, 5), padding='same', input_shape=x_train.shape[1:])
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_2_2(*args):
    idx = 2
    x_train = args[0]
    layers_list = args[idx]
    layers_list[0] = Conv2D(7, (7, 7), padding='same', input_shape=x_train.shape[1:])
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_3(*args):
    idx = 2
    x_train = args[0]
    layers_list = args[idx]
    layers_list = layers_list[2:]
    layers_list[0] = Conv2D(7, (5, 5), padding='same', input_shape=x_train.shape[1:])
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_4_1(*args):
    idx = 1
    learning_rate = 1 / (10 ** 1)
    params = list(args)
    params[idx] = learning_rate
    return params


@q_wrapper
def q_3_4_2(*args):
    idx = 1
    learning_rate = 1 / (10 ** 3)
    params = list(args)
    params[idx] = learning_rate
    return params


@q_wrapper
def q_3_4_3(*args):
    idx = 1
    learning_rate = 1 / (10 ** 4)
    params = list(args)
    params[idx] = learning_rate
    return params


@q_wrapper
def q_3_5_1(*args):
    idx = 2
    layers_list = args[idx]
    layers_list.pop(1)
    layers_list.insert(1, LeakyReLU(alpha=0.3))
    layers_list.pop(3)
    layers_list.insert(3, LeakyReLU(alpha=0.3))
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_5_2(*args):
    idx = 2
    layers_list = args[idx]
    layers_list[1] = Activation('tanh')
    layers_list[3] = Activation('tanh')
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_6_1(*args):
    idx = 3
    params = list(args)
    params[idx] = 'adagrad'
    return params


@q_wrapper
def q_3_6_2(*args):
    idx = 3
    params = list(args)
    params[idx] = 'sgd'
    return params


@q_wrapper
def q_3_7(*args):
    idx = 2
    layers_list = args[idx]
    layers_list.insert(0, BatchNormalization())
    params = list(args)
    params[idx] = layers_list
    return params


@q_wrapper
def q_3_8_1(*args):
    idx = 5
    batch_size = 4
    params = list(args)
    params[idx] = batch_size
    return params


@q_wrapper
def q_3_8_2(*args):
    idx = 5
    batch_size = 128
    params = list(args)
    params[idx] = batch_size
    return params


def q_3_10(*args):
    x_train, y_train, x_test, y_test = CifarKerasCNN.read_cifar_data()
    learning_rate = 0.01
    optimizer = 'Adam'
    loss_function = 'categorical_crossentropy'
    batch_size = 32
    manager = CifarMobileNetCNN(
        learning_rate=learning_rate,
        optimizer=optimizer,
        loss_function=loss_function,
        batch_size=batch_size,
    )
    out_dir = manager.fit(x_train, y_train)
    manager.test(x_test, y_test, out_dir=out_dir)


if __name__ == '__main__':
    # q_2()
    # q_3_0()
    # q_3_1_1()
    # q_3_1_2()
    q_3_2_1()
    # q_3_2_2()
    # q_3_3()
    # sleep(20 * 60)
    # q_3_4_1()
    # q_3_4_2()
    # q_3_4_3()
    # q_3_5_1()
    # q_3_5_2()
    # sleep(20 * 60)
    # q_3_6_1()
    # q_3_6_2()
    # q_3_7()
    # q_3_8_1()
    # q_3_8_2()
    # sleep(20 * 60)
    # q_3_10()
