import tensorflow as tf
from tensorflow_core.python.keras import Input
from tensorflow_core.python.keras.applications import MobileNetV2
from tensorflow_core.python.keras.layers import Dense, Flatten

from src.ex_3.cifar10 import CifarKerasCNN


class CifarMobileNetCNN(CifarKerasCNN):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        pre_trained_model = MobileNetV2(weights='imagenet', include_top=False)
        flatten_layer = Flatten()
        dense_layer = Dense(10, activation='softmax')

        input_images = Input(shape=(32, 32, 3), name='input_image')
        features = pre_trained_model(input_images)
        flatten_features = flatten_layer(features)
        final_outputs = dense_layer(flatten_features)

        model = tf.keras.Model(inputs=input_images, outputs=final_outputs)

        optimizer_dict = {
            'adagrad': tf.keras.optimizers.Adagrad,
            'adam': tf.keras.optimizers.Adam,
            'sgd': tf.keras.optimizers.SGD,
        }
        _optimizer = optimizer_dict.get(self.optimizer.lower())
        model.compile(
            loss=self.loss_function,
            optimizer=_optimizer(learning_rate=self.learning_rate),
            metrics=['accuracy'],
        )

        self.model = model
        self.trained_model = None
