import os

import numpy as np
import tensorflow as tf
from tensorflow.keras import layers

from src.base.utils import read_image_net
from src.ex_2.auto_encoder import AutoEncoder
from src.ex_2.mlp_result import MlpResult


class AutoEncoderKeras(AutoEncoder):
    SUB_RESULT_DIR = os.path.join(AutoEncoder.RESULT_DIR, 'ex_3')

    def __init__(self, activation=None, hidden_layers=None, alpha=None, theta=None, miu=None, batch_size=None,
                 normalize=None, *args, **kwargs):
        super().__init__(activation, hidden_layers, alpha, theta, miu, batch_size, normalize, *args, **kwargs)
        model = tf.keras.Sequential()
        for layer_size in self.hidden_layers:
            layer = layers.Dense(layer_size, activation='sigmoid')
            model.add(layer)
        sgd = tf.keras.optimizers.SGD(learning_rate=self.alpha, momentum=self.miu, nesterov=False)
        model.compile(loss='mean_squared_error', optimizer=sgd)
        self.model = model
        self.trained_model = None

    def fit(self, file_name, data_matrix=None, label_matrix=None):
        if data_matrix is None or label_matrix is None:
            data_matrix, label_matrix = self.read_data(file_name)
        output_layer = layers.Dense(label_matrix.shape[1], activation='sigmoid')
        self.model.add(output_layer)

        es = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10,
                                              restore_best_weights=True)
        trained_model = self.model.fit(
            data_matrix, label_matrix,
            validation_split=0.2,
            epochs=50000,
            batch_size=self.batch_size,
            verbose=1,
            callbacks=[es, ]
        )
        self.trained_model = trained_model
        weights = self.model.get_weights()
        out_dir = self._save_w_matrix(weights)
        mse_history = [trained_model.history['loss'], trained_model.history['val_loss']]
        result = MlpResult(
            activation=self.activation,
            hidden_layer=self.hidden_layers,
            alpha=self.alpha,
            miu=self.miu,
            batch_size=self.batch_size,
            weights=weights,
            mse_history=mse_history,
            epoch_number=len(trained_model.epoch),
            out_dir=out_dir,
        )
        return result

    def feed_forward(self, input_array, *args):
        predict = self.model.predict(np.array([input_array, ]))
        return predict.ravel()


if __name__ == '__main__':
    train_data_raw, test_data_raw = read_image_net()
    train_data = np.array([np.array(x).astype('float32') / 255 for x in train_data_raw])
    train_matrix = train_data.reshape(train_data.shape[0], np.prod(train_data.shape[1:]))
    test_data = np.array([np.array(x).astype('float32') / 255 for x in test_data_raw])
    test_matrix = test_data.reshape(test_data.shape[0], np.prod(test_data.shape[1:]))

    ANN = AutoEncoderKeras(hidden_layers=[1000, ])
    ANN.fit('train_6.csv', train_matrix, train_matrix)
