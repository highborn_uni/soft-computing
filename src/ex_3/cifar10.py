import os
import time
from contextlib import redirect_stdout
from datetime import datetime

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from tensorflow import keras
from tensorflow.keras.datasets import cifar10

from src.base.neural_network import NeuralNetwork


class CifarKerasCNN(NeuralNetwork):
    MODEL_NAME = 'cifar10-model.h5'
    SUMMARY_NAME = 'cifar10-model-summary.txt'

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    DATA_DIR = os.path.join(BASE_DIR, 'data')
    EX_3_DATA_DIR = os.path.join(DATA_DIR, 'ex_3')
    RESULT_DIR = os.path.join(BASE_DIR, 'statics')
    SUB_RESULT_DIR = os.path.join(RESULT_DIR, 'ex_3')

    DEFAULT_LEARNING_RATE = 0
    DEFAULT_LAYERS_LIST = []
    DEFAULT_OPTIMIZER = 'adam'
    DEFAULT_LOSS_FUNCTION = 'categorical_crossentropy'
    DEFAULT_BATCH_SIZE = None

    def __init__(self,
                 learning_rate=None,
                 layers_list=None,
                 optimizer=None,
                 loss_function=None,
                 batch_size=None,
                 *args, **kwargs):
        self.learning_rate = learning_rate if learning_rate else self.DEFAULT_LEARNING_RATE
        self.layers_list = layers_list if layers_list else self.DEFAULT_LAYERS_LIST
        self.optimizer = optimizer if optimizer else self.DEFAULT_OPTIMIZER
        self.loss_function = loss_function if loss_function else self.DEFAULT_LOSS_FUNCTION
        self.batch_size = batch_size if batch_size else self.DEFAULT_BATCH_SIZE

        model = keras.Sequential()
        for layer in self.layers_list:
            model.add(layer)

        optimizer_dict = {
            'adagrad': tf.keras.optimizers.Adagrad,
            'adam': tf.keras.optimizers.Adam,
            'sgd': tf.keras.optimizers.SGD,
        }
        _optimizer = optimizer_dict.get(self.optimizer.lower())
        model.compile(
            loss=self.loss_function,
            optimizer=_optimizer(learning_rate=self.learning_rate),
            metrics=['accuracy'],
        )
        self.model = model
        self.trained_model = None
        self.train_time = None

    def read_data(self, file_name=None, **kwargs):
        if file_name:
            raise NotImplementedError()
        else:
            return self.read_cifar_data(**kwargs)

    @staticmethod
    def read_batch_cifar_data(index=1):
        file_name = os.path.join(CifarKerasCNN.EX_3_DATA_DIR, f'data_batch_{index}')
        import pickle
        with open(file_name, 'rb') as fo:
            data_dict = pickle.load(fo, encoding='bytes')
        x_batch = data_dict[b'data'].reshape((len(data_dict[b'data']), 3, 32, 32)).transpose(0, 2, 3, 1)
        y_batch = data_dict[b'labels']
        return x_batch, y_batch

    @staticmethod
    def read_cifar_data(sample=False):
        num_classes = 10
        train, test = cifar10.load_data()
        if sample:
            train = CifarKerasCNN.read_batch_cifar_data(index=1)
        x_train, y_train = train
        x_test, y_test = test
        x_train = x_train.astype('float32') / 255
        y_train = keras.utils.to_categorical(y_train, num_classes)
        x_test = x_test.astype('float32') / 255
        y_test = keras.utils.to_categorical(y_test, num_classes)
        return x_train, y_train, x_test, y_test

    def fit(self, x_train, y_train):
        start = time.time()
        es = tf.keras.callbacks.EarlyStopping(
            monitor='val_loss',
            mode='min',
            verbose=1,
            patience=20,
            restore_best_weights=True
        )
        self.trained_model = self.model.fit(
            x_train, y_train,
            validation_split=0.2,
            epochs=1000,
            batch_size=self.batch_size,
            callbacks=[es, ],
        )
        self.train_time = time.time() - start
        out_dir, summary = self.save_model()
        self.plot_model(out_dir)
        return out_dir

    def plot_model(self, out_dir):
        t_mse, v_mse = self.trained_model.history['loss'], self.trained_model.history['val_loss']
        index = np.arange(len(v_mse))
        bar_width = 0.35
        opacity = 0.8
        plt.plot(index, t_mse, bar_width, alpha=opacity, color='#700c8e', label='train MSE')
        plt.plot(index, v_mse, bar_width, alpha=opacity, color='orange', label='validation MSE')
        plt.xlabel(f'Epoch N.\n total: {len(self.trained_model.epoch)}')
        plt.ylabel(f'Loss\nmin on Validation:{min(v_mse)}\nmin on train:{min(t_mse)}')
        title = 'Loss change plot'
        plt.title(title)
        labels = (str(x) for x in range(len(t_mse)))
        plt.xticks(index, labels)
        plt.legend()
        plt_name = os.path.join(out_dir, 'plot.png')
        plt.savefig(plt_name)
        plt.show()

    def save_model(self, model=None):
        if model is None:
            model = self.model
        if not os.path.isdir(self.RESULT_DIR):
            os.makedirs(self.RESULT_DIR)
        if not os.path.isdir(self.SUB_RESULT_DIR):
            os.makedirs(self.SUB_RESULT_DIR)
        class_name = self.__class__.__name__
        directory = class_name + datetime.now().strftime('-%Y%m%d%H%M%S')
        directory_full_path = os.path.join(self.SUB_RESULT_DIR, directory)
        os.mkdir(directory_full_path)
        model_path = os.path.join(directory_full_path, self.MODEL_NAME)
        summary_path = os.path.join(directory_full_path, self.SUMMARY_NAME)
        model.save(model_path)
        with open(summary_path, 'w+') as f:
            with redirect_stdout(f):
                model.summary()
        with open(summary_path, 'r') as f:
            summary = f.read()
        return directory_full_path, summary

    def load_model(self, directory_name: str):
        model_path = os.path.join(directory_name, self.MODEL_NAME)
        new_model = tf.keras.models.load_model(model_path)
        return new_model

    def set_model(self, model):
        self.model = model
        return self.model

    def test(self, x_test, y_test, out_dir):
        scores = self.model.evaluate(x_test, y_test, verbose=1)
        print('Test loss:', scores[0])
        print('Test accuracy:', scores[1])
        summary_path = os.path.join(out_dir, self.SUMMARY_NAME)
        with open(summary_path, 'a+') as f:
            f.write(f'\nTest loss: {scores[0]}')
            f.write(f'\nTest accuracy: {scores[1]}')
            f.write(f'\nTrain time: {self.train_time}')
            f.write(f'\nTrain epoch: {len(self.trained_model.epoch)}')
            f.write(f'\nOptimizer: {self.optimizer}')
            f.write(f'\nLearning rate: {self.learning_rate}')
            f.write(f'\nBatch size: {self.batch_size}')
        return scores
