class BasicResult:
    def __init__(self, true, false, na=0):
        self.true_predicts = true
        self.false_predicts = false
        self.na = na

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        output_str = (
            f'True predicts: {self.true_predicts_pct * 100} %\n'
            f'False predicts: {self.false_predicts_pct * 100} %\n'
            f'NAs: {self.na_pct * 100} %\n'
            f'out of: {self.total}\n'
        )
        return output_str

    @property
    def total(self) -> int:
        return sum([self.true_predicts, self.false_predicts, self.na])

    @property
    def na_pct(self) -> float:
        return self.na / self.total

    @property
    def true_predicts_pct(self) -> float:
        return self.true_predicts / self.total

    @property
    def false_predicts_pct(self) -> float:
        return self.false_predicts / self.total
