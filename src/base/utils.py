import glob
import os

from PIL import Image

import numpy as np


def normalize_data(x: np.ndarray):
    min_ = x.min()
    max_ = x.max()
    x = (x - min_) / (max_ - min_)
    return x


def sigmoid(x: np.ndarray) -> np.ndarray:
    _check_dim(x, 1)
    result = 1 / (1 + np.exp(-x))
    return result


def sigmoid_inverse(x: np.ndarray) -> np.ndarray:
    result = map(lambda a: a * (1 - a), x)
    return np.array(list(result))


def tanh(x: np.ndarray) -> np.ndarray:
    return np.tanh(x)


def tanh_inverse(x: np.ndarray) -> np.ndarray:
    result = map(lambda a: 1 - (a ** 2), x)
    return np.array(list(result))


def relu(x: np.ndarray) -> np.ndarray:
    _check_dim(x, 1)
    result = map(lambda a: a if a > 0 else 0, x)
    return np.array(list(result))


def relu_inverse(x: np.ndarray) -> np.ndarray:
    _check_dim(x, 1)
    result = map(lambda a: 1 if a > 0 else 0, x)
    return np.array(list(result))
    # return x


def read_image_net():
    base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    data_set = [
        list(),  # train data
        list(),  # test data
    ]
    path_list = [
        base_dir + '/data/ex_3/train_img/*.jpg',
        base_dir + '/data/ex_3/test_img/*.jpg',
    ]
    for idx, path in enumerate(path_list):
        for file_name in glob.glob(path):
            image = Image.open(file_name)
            data_set[idx].append(image)
    return data_set[0], data_set[1]


def _check_dim(x, d):
    if not isinstance(x, np.ndarray):
        raise ValueError('only numpy arrays are acceptable not {}'.format(type(x)))
    dim = x.shape
    for i in range(d):
        try:
            dim_, *dim = dim
        except ValueError:
            raise ValueError('Dimension Error!')
    if len(dim) is not 0:
        raise ValueError('Dimension Error!')
