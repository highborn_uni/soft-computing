class BinaryResult:
    def __init__(self, true_true, true_false, false_false, false_true, na):
        self.true_true = true_true
        self.true_false = true_false
        self.false_false = false_false
        self.false_true = false_true
        self.na = na

    def __str__(self):
        output_str = (
            f'True predicts: {self.true_predicts}\n'
            f'False predicts: {self.false_predicts}\n'
            f'NAs: {self.na}\n'
            f'out of: {self.total}\n'
        )
        print(output_str)

    @property
    def true_predicts(self) -> int:
        return sum([self.true_true, self.false_false])

    @property
    def false_predicts(self) -> int:
        return sum([self.true_false, self.false_true])

    @property
    def total(self) -> int:
        return sum([self.true_predicts, self.false_predicts, self.na])

    @property
    def true_true_pct(self) -> float:
        return self.true_true / self.total

    @property
    def true_false_pct(self) -> float:
        return self.true_false / self.total

    @property
    def false_false_pct(self) -> float:
        return self.false_false / self.total

    @property
    def false_true_pct(self) -> float:
        return self.false_true / self.total

    @property
    def na_pct(self) -> float:
        return self.na / self.total

    @property
    def true_predicts_pct(self) -> float:
        return self.true_predicts / self.total

    @property
    def false_predicts_pct(self) -> float:
        return self.false_predicts / self.total
