import numpy as np

from src.ex_1.single_layer_neural_network import SingleLayerNN


class HebNN(SingleLayerNN):
    def fit(self):
        input_matrix_array, label_matrix_array = self.read_data(directory=self.TRAIN_DATA_DIR)
        w_matrix = np.zeros((input_matrix_array.shape[1], len(self.LABEL_TRANSLATOR_DICT)))
        for i_index, _ in enumerate(input_matrix_array):
            expected_vector = label_matrix_array[i_index:i_index + 1]
            delta_w = np.matmul(np.transpose(input_matrix_array[i_index:i_index + 1]), expected_vector)
            w_matrix += delta_w

        train_result = self.make_result(input_matrix_array, label_matrix_array, w_matrix)
        print(str(train_result))

        input_matrix_array, label_matrix_array = self.read_data(directory=self.TEST_DATA_DIR)
        test_result = self.make_result(input_matrix_array, label_matrix_array, w_matrix)
        print(test_result)
