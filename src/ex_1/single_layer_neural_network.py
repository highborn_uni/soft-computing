import os
from abc import abstractmethod
from datetime import datetime
from random import shuffle
from time import sleep
from typing import Tuple

import numpy as np

from src.base.base_result import BasicResult
from src.base.neural_network import NeuralNetwork


class SingleLayerNN(NeuralNetwork):
    DEFAULT_ALPHA = 1
    DEFAULT_GAMA = 0
    DEFAULT_BIAS = 1
    DEFAULT_THETA = .2

    BASE_DIR = "./data/ex_1/"
    TRAIN_DATA_DIR = 'characters_train_set'
    TEST_DATA_DIR = 'characters_test_set'

    DATA_TRANSLATOR_DICT = {
        '#': 1.0,
        '.': -1.0,
    }
    LABEL_TRANSLATOR_DICT = {
        'A': 0,
        'B': 1,
        'C': 2,
        'D': 3,
        'E': 4,
        'J': 5,
        'K': 6,
    }

    def __init__(self, alpha=None, gama=None, theta=None) -> None:
        super().__init__()
        self.alpha = alpha if alpha else self.DEFAULT_ALPHA
        self.gama = gama if gama else self.DEFAULT_GAMA
        self.theta = theta if theta else self.DEFAULT_THETA

    def _class_extra(self):
        return ''

    def read_data(self, directory: str) -> Tuple[np.array, np.array]:
        if not isinstance(directory, str):
            raise ValueError("data directory is mandatory!")
        elif directory.startswith('/'):
            data_dir = directory
        else:
            data_dir = os.path.join(self.BASE_DIR, directory)

        os_listdir = os.listdir(data_dir)
        # shuffle(os_listdir)
        data_list = list()
        label_list = list()
        for filename in os_listdir:
            file = os.path.join(data_dir, filename)
            with open(file, 'r') as raw_data:
                t = list()
                for line in raw_data.readlines():
                    line = line.strip()
                    char_list = list(line)
                    item_list = list()
                    for item in char_list:
                        item_list.append(self.DATA_TRANSLATOR_DICT.get(item, 0))
                    t.extend(item_list)
            t.insert(0, self.DEFAULT_BIAS)
            eye_matrix_row_index = self.LABEL_TRANSLATOR_DICT.get(filename[0])
            correspond_eye_row = np.eye(len(self.LABEL_TRANSLATOR_DICT))[eye_matrix_row_index]
            negative_row = -1 * np.ones((len(self.LABEL_TRANSLATOR_DICT)))
            label_list.append(negative_row + (2 * correspond_eye_row))
            array = np.array(t)
            data_list.append(array)
        input_matrix_array = np.array(data_list)
        label_matrix_array = np.array(label_list)
        return input_matrix_array, label_matrix_array

    def initialize_weight(self, input_size, output_size):
        weight_matrix = np.random.uniform(
            low=-self.gama,
            high=self.gama,
            size=(input_size, output_size),
        )
        weight_matrix[0, :] = 1
        return weight_matrix

    def make_result(self, input_matrix_array, label_matrix_array, weight_matrix) -> BasicResult:
        true_predicts = 0
        false_predicts = 0
        for i_index in range(input_matrix_array.shape[0]):
            input_vector = input_matrix_array[i_index:i_index + 1]
            label_matrix = label_matrix_array[i_index]
            is_true, _ = self.feed_forward(input_vector, label_matrix, weight_matrix)
            if is_true:
                true_predicts += 1
            else:
                false_predicts += 1
        result = BasicResult(true_predicts, false_predicts)
        return result

    def _feed_forward(self, input_matrix, weight_matrix):
        """compute Y_in vector."""
        predict_matrix = np.matmul(input_matrix, weight_matrix)
        predict_vector = predict_matrix[0]
        return predict_vector

    def feed_forward(self, input_matrix, label_matrix, weight_matrix) -> Tuple:
        raw_predict_vector = self._feed_forward(input_matrix, weight_matrix)
        predict_vector = self.active_function(raw_predict_vector)
        is_true = False
        if label_matrix is not None:
            if (predict_vector == label_matrix).all():
                is_true = True
        return is_true, predict_vector

    def active_function(self, predict_vector):
        # step active function
        predict_vector = np.tanh(predict_vector)
        for i in range(len(predict_vector)):
            if predict_vector[i] < - self.theta:
                predict_vector[i] = -1
            elif predict_vector[i] > self.theta:
                predict_vector[i] = 1
            else:
                predict_vector[i] = 0
        return predict_vector

    def clean_workspace(self, weight_matrix, train_result: BasicResult, test_result: BasicResult):
        class_name = self.__class__.__name__
        file_name = class_name + datetime.now().strftime('%Y%m%d%H%M%S')
        sleep(1)
        os.mkdir('./var/results/{}'.format(file_name))
        result_prefix = './var/results/' + file_name
        with open(result_prefix + '/weights', 'wb+') as f:
            np.save(f, weight_matrix)
        with open(result_prefix + '/details', 'w+') as f:
            f.write("# train result:\n")
            f.write(str(train_result))
            f.write("\n# test result:\n")
            f.write(str(test_result))
            extra = self._class_extra()
            if extra:
                f.write('\n\n' + extra)

    @abstractmethod
    def fit(self):
        raise NotImplementedError()
