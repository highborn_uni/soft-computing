import numpy as np

from src.ex_1.perceptron import Perceptron


class Adaline(Perceptron):
    def learn(self, expected_matrix, input_matrix, weight_matrix, predict_vector, *args):
        delta_output = expected_matrix - predict_vector
        delta_w = np.matmul(np.transpose(input_matrix), delta_output)
        weight_matrix += self.alpha * delta_w
        return weight_matrix
