import json
from typing import Tuple

import numpy as np

from src.ex_1.single_layer_neural_network import SingleLayerNN


class Perceptron(SingleLayerNN):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.wrong_predict = True
        self.epoch_counter = 0

    def _class_extra(self):
        extra = {
            "alpha": self.alpha,
            "gama": self.gama,
            "theta": self.theta,
            "fitted at epoch": self.epoch_counter,
        }
        return json.dumps(extra)

    def fit(self, input_matrix_array=None, label_matrix_array=None):
        if input_matrix_array is None:
            input_matrix_array, label_matrix_array = self.read_data(directory=self.TRAIN_DATA_DIR)
        weight_matrix = self.initialize_weight(input_matrix_array.shape[1], label_matrix_array.shape[1])
        try:
            weight_matrix = self._fit(input_matrix_array, label_matrix_array, weight_matrix)
        except KeyboardInterrupt:
            self.epoch_counter *= -1  # means not fitted

        print(f'fitted at epoch: {self.epoch_counter}')
        train_result = self.make_result(input_matrix_array, label_matrix_array, weight_matrix)
        input_matrix_array, label_matrix_array = self.read_data(directory=self.TEST_DATA_DIR)
        test_result = self.make_result(input_matrix_array, label_matrix_array, weight_matrix)
        self.clean_workspace(weight_matrix, train_result, test_result)

    def _fit(self, input_matrix_array, label_matrix_array, weight_matrix):
        while self.is_fit():
            self.wrong_predict = False
            false_predicts = 0
            for i_index, _ in enumerate(input_matrix_array):
                expected_matrix = label_matrix_array[i_index:i_index + 1]
                input_matrix = input_matrix_array[i_index:i_index + 1]
                predicted, predict_vector = self.feed_forward(input_matrix, expected_matrix, weight_matrix)
                weight_matrix = self.learn(expected_matrix, input_matrix, weight_matrix, predict_vector, predicted)
                if not predicted:
                    self.wrong_predict = True
                    false_predicts += 1
            self.epoch_counter += 1
        return weight_matrix

    def learn(self, expected_matrix, input_matrix, weight_matrix, predict_vector, predicted):
        for i in range(predict_vector.shape[0]):
            # if predicted dont update by :
            if predict_vector[i] == expected_matrix[0, i]:
                predict_vector[i] = 0
            else:
                predict_vector[i] = expected_matrix[0, i]
        delta_w = np.matmul(np.transpose(input_matrix), np.array([predict_vector, ]))
        weight_matrix = weight_matrix + (self.alpha * delta_w)
        return weight_matrix

    def is_fit(self) -> bool:
        not_yet = True if self.wrong_predict else False
        return not_yet

    def read_data_(self, directory: str) -> Tuple[np.array, np.array]:
        """sample read data for test logic for 3 parameter and operator."""
        x = np.array(
            [
                [1, 1, 1, 1],
                [1, 1, -1, 1],
                [1, -1, -1, 1],
                [-1, -1, -1, 1],
                [-1, 1, -1, 1],
            ]
        )
        y = np.array([
            [1], [-1], [-1], [-1], [-1], [-1], [-1], [-1]
        ])
        return x, y
