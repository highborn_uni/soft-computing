import inspect
import os
from abc import abstractmethod
from datetime import datetime

import numpy as np


class GeneticAlgorithm:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    DATA_DIR = os.path.join(BASE_DIR, 'data')
    SUB_DATA_DIR = os.path.join(DATA_DIR, 'ex_4')
    RESULT_DIR = os.path.join(BASE_DIR, 'statics')
    SUB_RESULT_DIR = os.path.join(RESULT_DIR, 'ex_4')
    SUMMARY_NAME = 'summary.txt'

    def __init__(self, chromosome_size, population_size, mutation_factor, crossover_factor):
        self.chromosome_size = chromosome_size
        self.population_size = population_size
        self.mutation_factor = mutation_factor
        self.crossover_factor = crossover_factor

        self.best_chromosome = None
        self.best_chromosome_fitness = None
        self.history = {
            'max': list(),
            'mean': list(),
            'min': list(),
        }
        self.population = self.init_population()
        self.population_fitness = self.update_population_fitness()

    def update_population(self, population):
        self.population = population
        return self.population

    def update_population_fitness(self):
        self.population_fitness = [self.fitness(x) for x in self.population]
        return self.population_fitness

    def update_best_chromosome(self):
        best_idx = np.argmin(self.population_fitness)
        best_generation_fitness = self.population_fitness[best_idx]
        best_generation_chromosome = self.population[best_idx]
        if not self.best_chromosome_fitness or self.best_chromosome_fitness > best_generation_fitness:
            self.best_chromosome = best_generation_chromosome
            self.best_chromosome_fitness = best_generation_fitness
        return self.population_fitness

    def save_result(self):

        if not os.path.isdir(self.RESULT_DIR):
            os.makedirs(self.RESULT_DIR)
        if not os.path.isdir(self.SUB_RESULT_DIR):
            os.makedirs(self.SUB_RESULT_DIR)
        class_name = self.__class__.__name__
        directory = class_name + datetime.now().strftime('-%Y%m%d%H%M%S')
        directory_full_path = os.path.join(self.SUB_RESULT_DIR, directory)
        os.mkdir(directory_full_path)
        summary_path = os.path.join(directory_full_path, self.SUMMARY_NAME)
        with open(summary_path, 'w+') as f:
            summary = (
                f'run function: {inspect.stack()[2][3]}\n'
                f'run time: {datetime.now().strftime("%Y/%m/%d %H:%M:%S")}\n'
                f'best chromosome: {self.best_chromosome}\n'
                f'best cost: {self.best_chromosome_fitness}\n'
                f'number of generations: {len(self.history["min"])}\n'
                f'crossover factor: {self.crossover_factor}\n'
                f'mutation factor: {self.mutation_factor}\n'
            )
            f.write(summary)
        return directory_full_path

    @abstractmethod
    def init_population(self, *args, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def select(self, *args, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def crossover(self, *args, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def mutate(self, *args, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def fitness(self, *args, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def next_generation(self, *args, **kwargs):
        raise NotImplementedError()
