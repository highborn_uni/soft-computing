import inspect
import os
import random
from datetime import datetime
from typing import List, Set, Tuple

import numpy as np
import tsplib95
from matplotlib import pyplot as plt


class Ant:
    def __init__(self, node):
        self.node = node
        self.tour = [node]

    @property
    def tour_set(self) -> Set:
        return set(self.tour)

    @property
    def tour_list(self) -> List:
        return self.tour

    @property
    def tour_tuple(self) -> Tuple:
        return tuple(self.tour)

    def neighbours(self, neighbours):
        neighbour_set = set(neighbours)
        neighbour_set -= self.tour_set
        return neighbour_set

    def move(self, node):
        if node is None:
            raise ValueError()
        self.node = node
        self.tour.append(node)


class TSPAS:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    DATA_DIR = os.path.join(BASE_DIR, 'data')
    SUB_DATA_DIR = os.path.join(DATA_DIR, 'ex_4')
    RESULT_DIR = os.path.join(BASE_DIR, 'statics')
    SUB_RESULT_DIR = os.path.join(RESULT_DIR, 'ex_4')
    SUMMARY_NAME = 'summary.txt'

    DEFAULT_ALPHA = 1
    DEFAULT_BETA = 3
    DEFAULT_RHO = 0.1
    DEFAULT_XI = 0.01
    DEFAULT_Q0 = .3

    def __init__(self, ant_qty, node_qty, cost_matrix, **kwargs):
        self.ant_qty = ant_qty
        self.node_qty = node_qty
        self.node_list = list(range(self.node_qty))
        self.node_set = set(self.node_list)
        self.cost_matrix = cost_matrix
        self.alpha = kwargs.get('alpha', self.DEFAULT_ALPHA)
        self.beta = kwargs.get('beta', self.DEFAULT_BETA)
        self.rho = kwargs.get('rho', self.DEFAULT_RHO)
        self.xi = kwargs.get('xi', self.DEFAULT_XI)
        self.q0 = kwargs.get('q0', self.DEFAULT_XI)

        self.ant_list = None
        self.tao = None
        self.tao0 = None
        self.__init_ant_list()
        self.__init_tao()

        self.best_tour = None
        self.best_cost = None
        self.history = {
            'max': list(),
            'mean': list(),
            'min': list(),
        }

    def __init_ant_list(self) -> List[Ant]:
        ant_list = [Ant(0) for _ in range(self.ant_qty)]
        self.ant_list = ant_list
        return ant_list

    def __init_tao(self) -> Tuple:
        ant_qty = self.ant_qty
        node_qty = self.node_qty
        _, tour_cost = self.__nearest_neighbour_tour()
        tao0 = 1 / (node_qty * tour_cost)
        tao = np.ones_like(self.cost_matrix)
        tao = tao * (ant_qty / tour_cost)
        self.tao0 = tao0
        self.tao = tao
        return tao, tao0

    def __nearest_neighbour_tour(self):
        node_qty = self.node_qty
        best_tour = None
        best_cost = None
        for i in range(node_qty):
            tour = list()
            visited_cities = set()
            last_node = i
            while len(visited_cities) != node_qty:
                sorted_neighbours = np.argsort(self.cost_matrix[last_node])
                for node in sorted_neighbours:
                    if node in visited_cities:
                        continue
                    last_node = node
                    tour.append(last_node)
                    visited_cities.add(last_node)
                    break

            tour_cost = self.fitness(tour)
            if best_cost and best_cost < tour_cost:
                continue
            best_cost = tour_cost
            best_tour = tour
        return best_tour, best_cost

    def next_node(self, ant: Ant):
        tao = self.tao
        q0 = self.q0
        beta = self.beta
        cost_matrix = self.cost_matrix
        nominator_list = [tao[ant.node, x] * (1 / (cost_matrix[ant.node, x] ** beta)) if x not in ant.tour_set else 0
                          for x in range(self.node_qty)]
        q = np.random.random()
        if q > q0:
            next_node = np.argmax(nominator_list)
        else:
            denominator = np.sum(nominator_list)
            prob_list = [x / denominator for x in nominator_list]
            next_node = random.choices(self.node_list, prob_list, k=1)[0]
        return next_node

    def local_update(self):
        self.tao = self.tao * (1 - self.xi)

    def global_update(self):
        self.tao = self.tao * (1 - self.rho)
        tour_cost_list = [self.fitness(x.tour) for x in self.ant_list]
        best_cost_arg = np.argmin(tour_cost_list)
        best_ant = self.ant_list[best_cost_arg]
        best_cost = tour_cost_list[best_cost_arg]
        delta_tao = 1 / best_cost
        for i in range(self.node_qty - 1):
            start = best_ant.tour[i]
            end = best_ant.tour[i + 1]
            self.tao[start, end] += delta_tao
            self.tao[end, start] += delta_tao
        print(f'best tour: {best_ant.tour}')
        print(f'best tour cost: {best_cost}')
        if self.best_cost is None or best_cost < self.best_cost:
            self.best_cost = best_cost
            self.best_tour = best_ant.tour

        self.history['max'].append(np.max(tour_cost_list))
        self.history['mean'].append(np.mean(tour_cost_list))
        self.history['min'].append(best_cost)

    def fit(self):
        for k in range(100):
            self.__init_ant_list()
            for i in range(self.node_qty - 1):
                self.local_update()
                update_list = list()
                for ant in self.ant_list:
                    pre_node = ant.node
                    next_node = self.next_node(ant)
                    ant.move(next_node)
                    update_value = self.xi * self.tao0
                    update_list.append([pre_node, next_node, update_value])
                    update_list.append([next_node, pre_node, update_value])
                for record in update_list:
                    self.tao[record[0], record[1]] += record[2]
            self.global_update()

            tours_dict = dict()
            for ant in self.ant_list:
                key = ant.tour_tuple
                if tours_dict.get(key) is None:
                    tours_dict[key] = 0
                tours_dict[key] += 1
            print(k)
            tour_count = len(tours_dict)
            print(tour_count)
            if tour_count is 1:
                out_dir = self._save_result(k)
                self._plot(out_dir)
                break

    def fitness(self, tour: List, *args, **kwargs):
        cost = [self.cost_matrix[tour[i], tour[i + 1]] for i in range(self.node_qty - 1)]
        cost.append(self.cost_matrix[tour[-1], tour[0]])
        return np.sum(cost)

    def _save_result(self, k):
        if not os.path.isdir(self.RESULT_DIR):
            os.makedirs(self.RESULT_DIR)
        if not os.path.isdir(self.SUB_RESULT_DIR):
            os.makedirs(self.SUB_RESULT_DIR)
        class_name = self.__class__.__name__
        directory = class_name + datetime.now().strftime('-%Y%m%d%H%M%S')
        directory_full_path = os.path.join(self.SUB_RESULT_DIR, directory)
        os.mkdir(directory_full_path)
        summary_path = os.path.join(directory_full_path, self.SUMMARY_NAME)
        with open(summary_path, 'w+') as f:
            summary = (
                f'run function: {inspect.stack()[2][3]}\n'
                f'run time: {datetime.now().strftime("%Y/%m/%d %H:%M:%S")}\n'
                f'best tour: {self.best_tour}\n'
                f'best cost: {self.best_cost}\n'
                f'number of generations: {k}\n'
                f'number of ants: {self.ant_qty}\n'
                f'alpha factor: {self.alpha}\n'
                f'beta factor: {self.beta}\n'
                f'rho factor: {self.rho}\n'
                f'xi factor: {self.xi}\n'
                f'q0 factor: {self.q0}\n'
            )
            f.write(summary)
        return directory_full_path

    def _plot(self, out_dir=None):
        history_max = self.history['max']
        history_mean = self.history['mean']
        history_min = self.history['min']
        generations = np.arange(len(history_max))
        bar_width = 0.35
        opacity = 0.8
        plt.plot(generations, history_max, bar_width, alpha=opacity, color='#700c8e')
        plt.plot(generations, history_mean, bar_width, alpha=opacity, color='orange')
        plt.plot(generations, history_min, bar_width, alpha=opacity, color='blue')
        plt.xlabel(f'Iterations\n total: {max(generations)}')
        plt.ylabel(f'min cost:{min(history_min)}')
        plt.title('Iteration fitness')
        plt.tight_layout()

        if out_dir:
            plt_name = os.path.join(out_dir, 'plot.png')
            plt.savefig(plt_name)

        plt.show()

    @classmethod
    def init_cost_matrix(cls, file_name: str):
        """fix following issue before using library:
        .../site-packages/tsplib95/distances.py:122
        modify TYPES translate dict to be as what given bellow:
            TYPES = {
                ...
                'GEO': geographical,
                ...
            }
        """
        file_path = os.path.join(cls.SUB_DATA_DIR, file_name)
        problem = tsplib95.utils.load_problem(file_path)
        cost_matrix = np.zeros((problem.dimension, problem.dimension))
        for i in range(problem.dimension):
            for j in range(problem.dimension):
                cost_matrix[i, j] = problem.wfunc(i + 1, j + 1)
        return cost_matrix


if __name__ == '__main__':
    _cost_matrix = TSPAS.init_cost_matrix('ali.tsp')
    manager = TSPAS(
        ant_qty=29 * 10,
        node_qty=29,
        cost_matrix=_cost_matrix,
    )
    manager.fit()
