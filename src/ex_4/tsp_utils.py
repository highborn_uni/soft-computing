import numpy as np


class TSPUtils:
    @classmethod
    def euclidean_distance(cls, a, b):
        """2.1 Euclidean distance (L2-metric)
        http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/tsp95.pdf"""
        xd = a[0] - b[0]
        yd = a[1] - b[1]
        distance = round(np.sqrt(xd ** 2 + yd ** 2))
        return distance

    @classmethod
    def geographic_distance(cls, a, b):
        """2.4 Geographical distance
        http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/tsp95.pdf"""
        a_lat, a_long = cls.convert_to_radians(a)
        b_lat, b_long = cls.convert_to_radians(b)
        rrr = 6378.388
        q1 = np.math.cos(a_long - b_long)
        q2 = np.math.cos(a_lat - b_lat)
        q3 = np.math.cos(a_lat + b_lat)
        distance = round(rrr * np.math.acos(.5 * ((1 + q1) * q2 - (1 - q1) * q3)) + 1)
        return distance

    @classmethod
    def convert_to_radians(cls, x):
        degrees = round(x[0])
        minutes = x[0] - degrees
        lat = np.math.pi * (degrees + ((5 * minutes) / 3)) / 180
        degrees = round(x[1])
        minutes = x[1] - degrees
        long = np.math.pi * (degrees + ((5 * minutes) / 3)) / 180
        return lat, long
