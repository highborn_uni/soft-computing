import os
import random
import re
from typing import List, Tuple

import numpy as np
import tsplib95
from matplotlib import pyplot as plt

from src.ex_4.genetic_algorithm import GeneticAlgorithm
from src.ex_4.tsp_utils import TSPUtils


class TSPGA(GeneticAlgorithm):
    def __init__(self, chromosome_size, population_size, mutation_factor, crossover_factor, *args, **kwargs):
        self.file_name = kwargs.get('file_name', None)
        self.metric = kwargs.get('metric', None)
        self.margin = kwargs.get('margin', None)

        use_library = False if self.metric else True
        self.cost_matrix = self.init_cost_matrix(use_library)
        super().__init__(chromosome_size, population_size, mutation_factor, crossover_factor)

    def init_population(self, *args, **kwargs):
        self.population = [list(np.random.permutation(self.chromosome_size)) for _ in range(self.population_size)]
        return self.population

    def select(self, qty, population=None, *args, **kwargs):
        tournament_factor = 3
        if population:
            population_fitness = [self.fitness(x) for x in population]
        else:
            population = list(self.population)
            population_fitness = list(self.population_fitness)

        selection = list()
        candidate_list = list(zip(population, population_fitness))
        for i in range(qty):
            tournament = random.sample(candidate_list, tournament_factor)
            tournament = sorted(tournament, key=lambda x: x[1])
            candidate = tournament[0]
            candidate_list.remove(candidate)
            selection.append(candidate[0])
        return selection

    def crossover(self, selection, *args, **kwargs):
        offspring = list()
        for i in range(0, len(selection), 2):
            a, b = self.cycle_recombination(selection[i], selection[i + 1])
            a, b = self.mutate(a), self.mutate(b)
            offspring.extend([a, b])
        return offspring

    def cycle_recombination(self, x: List, y: List) -> Tuple[List, List]:
        a, b = list(x), list(y)
        visited = set()
        loop = list()
        loop_list = list()
        for i in range(self.chromosome_size):
            idx = i
            while True:
                if idx in visited:
                    if loop:
                        loop_list.append(loop)
                        loop = list()
                    break
                else:
                    visited.add(idx)
                    loop.append(idx)
                    idx = x.index(y[idx])
        for loop in loop_list:
            a, b = b, a
            for idx in loop:
                a[idx] = x[idx]
                b[idx] = y[idx]
        return a, b

    def mutate(self, chromosome, *args, **kwargs):
        gene_mutation = np.random.random(self.chromosome_size)
        for i in range(self.chromosome_size):
            if gene_mutation[i] <= self.mutation_factor:
                j = np.random.randint(i, self.chromosome_size)
                chromosome[i + 1:j + 1], chromosome[i] = chromosome[i: j], chromosome[j]
        return chromosome

    def fitness(self, chromosome, *args, **kwargs):
        cost = [self.cost_matrix[chromosome[i], chromosome[i + 1]] for i in range(self.chromosome_size - 1)]
        cost.append(self.cost_matrix[chromosome[-1], chromosome[0]])
        return np.sum(cost)

    def next_generation(self, *args, **kwargs):
        pass

    def init_cost_matrix(self, use_library=True):
        """Create cost matrix for fitness function."""
        if use_library:
            cost_matrix = self._init_cost_matrix_from_tsplib()
        else:
            cost_matrix = self._init_cost_matrix()
        return cost_matrix

    def _init_cost_matrix_from_tsplib(self):
        """fix following issue before using library:
        .../site-packages/tsplib95/distances.py:122
        modify TYPES translate dict to be as what given bellow:
            TYPES = {
                ...
                'GEO': geographical,
                ...
            }
        """
        file_path = os.path.join(self.SUB_DATA_DIR, self.file_name)
        problem = tsplib95.utils.load_problem(file_path)
        cost_matrix = np.zeros((problem.dimension, problem.dimension))
        for i in range(problem.dimension):
            for j in range(problem.dimension):
                if file_path.endswith('atsp'):
                    cost_matrix[i, j] = problem.wfunc(i, j)
                else:
                    cost_matrix[i, j] = problem.wfunc(i + 1, j + 1)
        return cost_matrix

    def _init_cost_matrix(self):
        file_path = os.path.join(self.SUB_DATA_DIR, self.file_name)
        with open(file_path, 'r') as f:
            data_set = list()
            for line in f:
                data = re.sub(' +', ' ', line.strip()).split(' ')
                data = [float(x) for x in data]
                data_set.append(data)
        number_of_nodes = len(data_set)
        if self.metric == 'abs':
            number_of_nodes += 1
        shape_of_cost_matrix = (number_of_nodes, number_of_nodes)
        cost_matrix = np.zeros(shape_of_cost_matrix)
        if self.metric in ('eu', 'geo'):
            if self.metric == 'eu':
                cost_function = TSPUtils.euclidean_distance
            elif self.metric == 'geo':
                cost_function = TSPUtils.geographic_distance
            else:
                raise ValueError()
            for i in range(number_of_nodes):
                point_a = data_set[i][1:]
                for j in range(i + 1, number_of_nodes):
                    point_b = data_set[j][1:]
                    cost_matrix[i, j] = cost_function(point_a, point_b)
        else:
            for i in range(number_of_nodes - 1):
                cost_matrix[i, i + 1:] = np.array(data_set[i])
        cost_matrix = cost_matrix + cost_matrix.T  # due to symmetry characteristic of TSP
        return cost_matrix

    def fit(self):
        while True:
            self.history['max'].append(np.max(self.population_fitness))
            self.history['mean'].append(np.mean(self.population_fitness))
            self.history['min'].append(np.min(self.population_fitness))

            _lambda = int(self.population_size * round(1 - self.crossover_factor, 2))
            _miu = self.population_size - _lambda
            _lambda_population = self.select(_lambda)
            # 1. select parents
            parents = self.select(_miu)
            # 2. crossover + mutation
            _miu_population = self.crossover(parents)
            # 3. replacement
            new_population = _lambda_population + _miu_population
            # 4. fitness
            self.update_population(new_population)
            self.update_population_fitness()
            self.update_best_chromosome()
            if self.is_fit():
                break
        print(f'best answer: {self.best_chromosome}\nbest cost: {self.best_chromosome_fitness}')
        print(f'number of generations: {len(self.history["mean"])}')
        out_dir = self.save_result()
        self.plot(out_dir)

    def plot(self, out_dir=None):
        history_max = self.history['max']
        history_mean = self.history['mean']
        history_min = self.history['min']
        generations = np.arange(len(history_max))
        bar_width = 0.35
        opacity = 0.8
        plt.plot(generations, history_max, bar_width, alpha=opacity, color='#700c8e')
        plt.plot(generations, history_mean, bar_width, alpha=opacity, color='orange')
        plt.plot(generations, history_min, bar_width, alpha=opacity, color='blue')
        plt.xlabel(f'Generations\n total: {max(generations)}')
        plt.ylabel(f'MSE\nmin cost:{min(history_min)}')
        plt.title('generation fitness')
        plt.tight_layout()

        if out_dir:
            plt_name = os.path.join(out_dir, 'plot.png')
            plt.savefig(plt_name)

        plt.show()

    def is_fit(self):
        fitted = False
        if np.min(self.history['min']) < np.min(self.history['min'][-self.margin:]):
            fitted = True
        if len(self.history['min']) > 10000:
            fitted = True

        return fitted
