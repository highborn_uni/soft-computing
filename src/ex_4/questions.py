from src.ex_4.tsp_as import TSPAS
from src.ex_4.tsp_ga import TSPGA


def q_2_3():
    for i in [.02, .03, .05]:
        for j in [.9, .8]:
            for p in [100, 200]:
                model = TSPGA(
                    chromosome_size=131,
                    population_size=p,
                    mutation_factor=i,
                    crossover_factor=j,
                    margin=1000,
                    file_name='ftv170.atsp')
                model.fit()


def q_3():
    for i in ['ali535.tsp']:
        cost_matrix = TSPAS.init_cost_matrix(i)
        manager = TSPAS(
            ant_qty=535 * 1,
            node_qty=535,
            cost_matrix=cost_matrix,
        )
        manager.fit()


if __name__ == '__main__':
    q_3()
