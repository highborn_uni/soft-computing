import os
import time
from typing import Tuple

import numpy as np
import pandas as pd

from src.base.utils import normalize_data
from src.ex_2.mlp_result import MlpResult
from src.ex_2.multi_layer_neural_network import MultiLayerNN


class MultiLayerPerceptron(MultiLayerNN):
    @classmethod
    def read_data(cls, file_name: str, normalize=True) -> Tuple[np.ndarray, np.ndarray]:
        file_path = os.path.join(cls.EX_2_DATA_DIR, file_name)
        df = pd.read_csv(file_path)
        data_matrix = np.array(df.iloc[:, :-1])
        label_matrix = np.array(df.iloc[:, -1:])
        if normalize:
            data_matrix = normalize_data(data_matrix)
        return data_matrix, label_matrix

    def fit(self, file_name):
        data_matrix, label_array = self.read_data(file_name)
        eighty_percent_index = data_matrix.shape[0] * 80 // 100
        # train:
        t_matrix = data_matrix[:eighty_percent_index]
        tl_matrix = label_array[:eighty_percent_index]
        # validation:
        v_matrix = data_matrix[eighty_percent_index:]
        vl_matrix = label_array[eighty_percent_index:]

        result = self._fit(t_matrix, tl_matrix, v_matrix, vl_matrix)

        return result

    def _fit(self, t_matrix, tl_matrix, v_matrix, vl_matrix):
        input_count = t_matrix.shape[1]
        output_count = tl_matrix.shape[1]
        network_shape_list = [input_count, *self.hidden_layers, output_count]
        w_matrix_list = self.initialize_weight_matrix(network_shape_list=network_shape_list)
        delta_w_prim = [np.zeros_like(x) for x in w_matrix_list]

        t_mse_history = list()
        v_mse_history = list()
        best_val_mse = 9999999
        best_weight_matrix = None
        best_epoch = 0
        for epoch in range(self.INF_EPOCH):
            start_time = time.time()
            t_epoch_mse_list = list()
            for j in range(0, t_matrix.shape[0], self.batch_size):
                batch_delta_w_list = list()
                for i in range(self.batch_size):
                    idx = j + i
                    if idx + 1 > len(t_matrix):
                        break
                    # step 1: feed forward network
                    ff_matrix_list = self.feed_forward(t_matrix[idx], w_matrix_list)
                    # step 2: back propagate Error
                    error = tl_matrix[idx] - ff_matrix_list[-1]
                    t_epoch_mse_list.append(np.sum(error ** 2))
                    bp_matrix_list = self.back_propagate(error, ff_matrix_list, w_matrix_list)
                    # step 3: update weight matrices based on propagated errors
                    delta_w_list = self.learn(bp_matrix_list, ff_matrix_list, delta_w_prim)
                    batch_delta_w_list.append(delta_w_list)
                w_matrix_list, delta_w_prim = self.update_weights(w_matrix_list, batch_delta_w_list)

            # calculate mse on validation data for stop condition
            v_epoch_mse_list = list()
            for j in range(v_matrix.shape[0]):
                ff_matrix_list = self.feed_forward(v_matrix[j], w_matrix_list)
                error = vl_matrix[j] - ff_matrix_list[-1]
                v_epoch_mse_list.append(np.sum(error ** 2))
            t_mse_history.append(np.average(t_epoch_mse_list))
            v_mse_history.append(np.average(v_epoch_mse_list))

            # mse_history = [t_mse_history, v_mse_history]
            # self.save_state_of_system(w_matrix_list, mse_history)
            # epoch_time = time.time() - start_time
            # print(f'{epoch}\t epoch time:\t{epoch_time}')
            # alternate end condition
            if self.is_fit(v_mse_history, best_val_mse):
                break

            # check for best value of weight matrix
            # print(best_val_mse - v_mse_history[-1])
            if best_val_mse > v_mse_history[-1]:
                # print('+')
                best_val_mse = v_mse_history[-1]
                best_weight_matrix = w_matrix_list
                best_epoch = epoch

        out_dir = self._save_w_matrix(best_weight_matrix)
        mse_history = [t_mse_history, v_mse_history]
        result = MlpResult(
            activation=self.activation,
            hidden_layer=self.hidden_layers,
            alpha=self.alpha,
            miu=self.miu,
            batch_size=self.batch_size,
            weights=best_weight_matrix,
            mse_history=mse_history,
            epoch_number=best_epoch,
            out_dir=out_dir,
        )
        return result

    def feed_forward(self, input_array, w_matrix_list):
        ff_matrix_list = [input_array]
        ff_matrix_list.extend([None] * self.layers_count)
        for i in range(self.layers_count):
            input_with_bias = np.append(ff_matrix_list[i], 1)
            raw_output = np.matmul(input_with_bias, w_matrix_list[i])
            ff_matrix_list[i + 1] = self.activate(raw_output, 0)
        return ff_matrix_list

    def back_propagate(self, error, ff_matrix_list, w_matrix_list):
        hidden_layer_count = self.layers_count - 1
        bp_matrix_list = [None] * hidden_layer_count
        bp_matrix_list.append(error)
        for i in reversed(range(hidden_layer_count)):
            neuron_income_error = np.matmul(bp_matrix_list[i + 1], w_matrix_list[i + 1].T)
            ff_matrix = np.array(ff_matrix_list[i + 1])
            ff_matrix = np.append(ff_matrix, 1)
            bp_matrix_list[i] = neuron_income_error * self.activate(ff_matrix, inverse=True)
            bp_matrix_list[i] = np.delete(bp_matrix_list[i], -1)
        return bp_matrix_list

    def learn(self, bp_matrix_list, ff_matrix_list, delta_w_prim):
        delta_w_list = list()
        for i in range(self.layers_count):
            delta_array = np.array(bp_matrix_list[i])
            delta_matrix = np.array([delta_array])
            input_array = np.append(ff_matrix_list[i], 1)
            input_matrix = np.array([input_array])
            current_delta_w = (np.matmul(input_matrix.T, delta_matrix) * self.alpha)
            momentum = self.miu * delta_w_prim[i]
            delta_w = current_delta_w + momentum
            delta_w_list.append(delta_w)
        return delta_w_list

    def update_weights(self, w_matrix_list, batch_delta_w_list):
        delta_w = np.sum(batch_delta_w_list, axis=0) / self.batch_size
        for i in range(self.layers_count):
            w_matrix_list[i] = w_matrix_list[i] + delta_w[i]
        return w_matrix_list, delta_w

    def is_fit(self, v_mse_history, best_v_mse) -> bool:
        response = False
        if self.SAME_EPOCH:
            if len(v_mse_history) > self.MAX_EPOCH:
                response = True
        else:
            if len(v_mse_history) > self.MIN_EPOCH:
                min_error_of_last_5_epochs = min(v_mse_history[-5:])
                max_error_of_second_last_5_epochs = max(v_mse_history[-10:-5])
                if max_error_of_second_last_5_epochs < min_error_of_last_5_epochs:
                    response = True
                min_error_of_last_10_epochs = min(v_mse_history[-10:])
                if min_error_of_last_10_epochs > best_v_mse:
                    response = True
                if best_v_mse - min_error_of_last_10_epochs < 10 ** (-5):
                    response = True
        return response
