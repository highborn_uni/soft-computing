from typing import Tuple

import numpy as np

from src.ex_2.multi_layer_perceptron import MultiLayerPerceptron


class AutoEncoder(MultiLayerPerceptron):
    @classmethod
    def read_data(cls, file_name: str, normalize=True) -> Tuple[np.ndarray, np.ndarray]:
        data_matrix, _ = super().read_data(file_name, normalize)
        return data_matrix, data_matrix
