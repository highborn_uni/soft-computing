import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

from src.ex_2.auto_encoder import AutoEncoder


class AutoEncoderClassifier:
    def __init__(self, ann: AutoEncoder, dir_name, theta=None):
        w_matrix_list = ann.load_w_matrix(dir_name)

        self.ann = ann
        self.theta = theta
        self.w_matrix_list = w_matrix_list

    def classify(self, data_matrix, crisp=False):
        mse_array = np.zeros((data_matrix.shape[0],))
        for idx in range(data_matrix.shape[0]):
            ff_matrix_list = self.feed_forward(data_matrix[idx])
            loss = tf.keras.losses.mean_squared_error(data_matrix[idx], ff_matrix_list)
            mse_array[idx] = loss
            # error = data_matrix[idx] - ff_matrix_list[-1]
            # mse_array[idx] = np.sum(error ** 2)
        if crisp:
            mse_array[mse_array < self.theta] = 0
            mse_array[mse_array > self.theta] = 1
        return mse_array

    def feed_forward(self, input_array):
        return self.ann.feed_forward(input_array, self.w_matrix_list)

    def fit(self, data_matrix, label_array):
        roc_list = [(0, 0), ]
        best_theta = 0
        best_f1_score = 0
        best_predict = None
        raw_predict_array = self.classify(data_matrix)
        rpa = np.sort(raw_predict_array)
        theta_range = [(rpa[i - 1] + rpa[i]) / 2 for i in range(1, rpa.shape[0])]
        for theta in theta_range:
            predict_array = np.array(raw_predict_array)
            predict_array[predict_array < theta] = 0
            predict_array[predict_array > theta] = 1

            accuracy, f1_score, fallout, precision, recall = self.make_result(label_array, predict_array)

            roc_list.append((recall, fallout))
            if f1_score > best_f1_score:
                best_theta = theta
                best_f1_score = f1_score
                best_predict = predict_array
        roc_list.append((1, 1))
        self.plot_roc(roc_list)

        self.make_result(label_array, best_predict, verbose=True)
        self.theta = best_theta
        return best_theta

    def make_result(self, label_array, predict_array, verbose=False):
        p_positive = np.where(predict_array == 0)[0]
        g_positive = np.where(label_array == 0)[0]
        p_negative = np.where(predict_array == 1)[0]
        g_negative = np.where(label_array == 1)[0]

        true_positive = np.intersect1d(p_positive, g_positive)
        false_positive = np.intersect1d(p_positive, g_negative)
        true_negative = np.intersect1d(p_negative, g_negative)
        false_negative = np.intersect1d(p_negative, g_positive)

        tpq = true_positive.shape[0]
        tnq = true_negative.shape[0]
        fpq = false_positive.shape[0]
        fnq = false_negative.shape[0]

        accuracy = (tpq + tnq) / predict_array.shape[0]
        precision = tpq / (tpq + fpq) if tpq else 0
        recall = tpq / (tpq + fnq) if tpq else 0
        fallout = fpq / (tnq + fpq) if fpq else 0
        f1_score = 2 * (precision * recall) / (precision + recall) if precision + recall else 0

        if verbose:
            print(f'theta: {self.theta}')
            print(f'F1 score: {f1_score}')
            print(f'Accuracy: {accuracy}')
            print(f'Precision: {precision}')
            print(f'Recall: {recall}')
        return accuracy, f1_score, fallout, precision, recall

    def plot_roc(self, roc_list):
        plt.plot(
            (0, 0, 1),
            (0, 1, 1),
            alpha=0.5, color='orange')
        plt.plot(
            (0, 1),
            (0, 1),
            alpha=0.3, color='orange')
        plt.plot(
            [x[1] for x in roc_list],
            [x[0] for x in roc_list],
            alpha=0.8, color='#700c8e')
        plt.xlabel('fallout')
        plt.ylabel('recall')
        title = 'roc curve'
        plt.title(title)
        plt.show()
