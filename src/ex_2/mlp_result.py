import numpy as np


class MlpResult:
    def __init__(self,
                 activation,
                 hidden_layer,
                 alpha,
                 miu,
                 batch_size,
                 weights,
                 mse_history,
                 epoch_number,
                 out_dir,
                 error_rate=None):
        self.activation = activation
        self.hidden_layer = hidden_layer
        self.alpha = alpha
        self.miu = miu
        self.batch_size = batch_size
        self.weights = weights
        self.mse_history = mse_history
        self.epoch_number = epoch_number
        self.out_dir = out_dir
        self.error_rate = error_rate

    def __str__(self):
        result_values = [
            f'activation: {self.activation}',
            f'hidden layers: {self.hidden_layer}',
            f'alpha: {self.alpha}',
            f'miu: {self.miu}',
            f'batch size: {self.batch_size}',
            f'epoch number: {self.epoch_number}',
            f'train mse: {self.train_mse}',
            f'validation mse: {self.validation_mse}',
            f'error rate: {self.error_rate}',
            f'epoch number: {self.epoch_number}',
        ]
        text = '\n'.join(result_values)
        return text

    def __repr__(self):
        return self.__str__()

    @property
    def train_mse(self):
        return np.amin(self.mse_history[0])

    @property
    def validation_mse(self):
        return np.amin(self.mse_history[1])
