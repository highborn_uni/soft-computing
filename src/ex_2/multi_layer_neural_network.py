import os
from datetime import datetime
from typing import List

import numpy as np

from src.base.neural_network import NeuralNetwork
from src.base.utils import sigmoid, tanh, relu, sigmoid_inverse, tanh_inverse, relu_inverse


class MultiLayerNN(NeuralNetwork):
    SAME_EPOCH = False
    INF_EPOCH = 10000
    MAX_EPOCH = 100
    MIN_EPOCH = 30
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    DATA_DIR = os.path.join(BASE_DIR, 'data')
    EX_2_DATA_DIR = os.path.join(DATA_DIR, 'ex_2')
    VAR_DIR = os.path.join(BASE_DIR, 'var')
    RESULT_DIR = os.path.join(VAR_DIR, 'results')
    SUB_RESULT_DIR = os.path.join(RESULT_DIR, 'ex_2')
    W_MATRIX_FILE_NAME = 'w_matrix'

    DEFAULT_ACTIVATION = 0
    DEFAULT_HIDDEN_LAYERS = [64]
    DEFAULT_ALPHA = 0.001
    DEFAULT_THETA = 0
    DEFAULT_MIU = 0
    DEFAULT_BATCH_SIZE = 1
    DEFAULT_GAMMA = .3
    DEFAULT_NORMALIZE = True

    def __init__(self,
                 activation=None,
                 hidden_layers=None,
                 alpha=None,
                 theta=None,
                 miu=None,
                 batch_size=None,
                 normalize=None,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self.activation = activation if activation else self.DEFAULT_ACTIVATION
        self.hidden_layers = hidden_layers if hidden_layers else self.DEFAULT_HIDDEN_LAYERS
        self.alpha = alpha if alpha else self.DEFAULT_ALPHA
        self.theta = theta if theta else self.DEFAULT_THETA
        self.miu = miu if miu else self.DEFAULT_MIU
        self.batch_size = batch_size if batch_size else self.DEFAULT_BATCH_SIZE
        self.normalize = normalize if normalize else self.DEFAULT_NORMALIZE

        self.layers_count = len(self.hidden_layers) + 1

    def initialize_weight_matrix(self, network_shape_list: List[int], bias_included=True) -> List[np.ndarray]:
        try:
            dir_list = sorted(os.listdir(self.W_MATRIX_FILE_NAME))
            if len(dir_list) is 0:
                raise FileNotFoundError
            file_name = dir_list[-1]
            with open(os.path.join(self.W_MATRIX_FILE_NAME, file_name), 'rb') as f:
                w_matrix_list = list(np.load(f))
        except FileNotFoundError:
            w_matrix_list = list()
            for i in range(len(network_shape_list) - 1):
                left_neuron_size = network_shape_list[i]
                right_neuron_size = network_shape_list[i + 1]
                if bias_included:
                    left_neuron_size += 1
                weight_matrix_size = (left_neuron_size, right_neuron_size)
                w_matrix_list.append(
                    np.random.uniform(
                        low=-self.DEFAULT_GAMMA,
                        high=self.DEFAULT_GAMMA,
                        size=weight_matrix_size,
                    )
                )
        return w_matrix_list

    def activate(self, x, inverse=False):
        af_dict = {
            0: sigmoid,
            1: tanh,
            2: relu,
        }
        af_inverse_dict = {
            0: sigmoid_inverse,
            1: tanh_inverse,
            2: relu_inverse,
        }
        if inverse:
            result = af_inverse_dict[self.activation](x)
        else:
            result = af_dict[self.activation](x)
        return result

    def load_w_matrix(self, file_path: str) -> List[np.ndarray]:
        if file_path.startswith('/'):
            file_full_path = file_path
        else:
            self.__check_directory_architecture()
            file_full_path = os.path.join(self.SUB_RESULT_DIR, file_path, self.W_MATRIX_FILE_NAME)
            file_full_path = file_full_path + '.npy'
        weight_matrix_list = list(np.load(file_full_path, allow_pickle=True))
        return weight_matrix_list

    def _save_w_matrix(self, w_matrix: List[np.ndarray]) -> str:
        self.__create_result_directory()
        class_name = self.__class__.__name__
        directory = class_name + datetime.now().strftime('-%Y%m%d%H%M%S')
        directory_full_path = os.path.join(self.SUB_RESULT_DIR, directory)
        os.mkdir(directory_full_path)
        file_full_path = os.path.join(directory_full_path, self.W_MATRIX_FILE_NAME)
        np.save(file_full_path, w_matrix, allow_pickle=True)
        return directory

    def __check_directory_architecture(self) -> None:
        assert os.path.exists(self.DATA_DIR)
        assert os.path.exists(self.EX_2_DATA_DIR)
        assert os.path.exists(self.VAR_DIR)
        assert os.path.exists(self.RESULT_DIR)
        assert os.path.exists(self.SUB_RESULT_DIR)

    def __create_result_directory(self) -> None:
        required_dir_list = [
            self.VAR_DIR,
            self.RESULT_DIR,
            self.SUB_RESULT_DIR,
        ]
        for required_dir in required_dir_list:
            if not os.path.exists(required_dir):
                os.mkdir(required_dir)
