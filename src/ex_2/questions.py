import random
import sys
import traceback
from functools import wraps

import numpy as np
from matplotlib import pyplot as plt

from src.ex_2.auto_encoder import AutoEncoder
from src.ex_2.auto_encoder_classifier import AutoEncoderClassifier
from src.ex_2.mlp_result import MlpResult
from src.ex_2.multi_layer_perceptron import MultiLayerPerceptron

DEBUG_MODE = 0


def q_wrapper(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        try:
            result_list = list()
            data, _ = AutoEncoder.read_data('train_6.csv')
            idx = random.randint(0, 200)
            show_sample(data[idx])
            f(result_list, data[idx], *args, **kwargs)
            print('*' * 10, idx, sep='\n')
        except Exception as e:
            if DEBUG_MODE is True:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback,
                                          limit=2, file=sys.stdout)
            else:
                raise e

    return wrap


def _plot_mse(result: MlpResult):
    t_mse, v_mse = result.mse_history
    index = np.arange(len(v_mse))
    bar_width = 0.35
    opacity = 0.8
    plt.plot(index, t_mse, bar_width, alpha=opacity, color='#700c8e', label='train MSE')
    plt.plot(index, v_mse, bar_width, alpha=opacity, color='orange', label='validation MSE')
    plt.xlabel(f'Epoch N.\n total: {result.epoch_number}')
    plt.ylabel(f'MSE\nmin on Validation:{min(v_mse)}\nmin on train:{min(t_mse)}')
    title = _make_title(result)
    plt.title(title)
    labels = (str(x) for x in range(len(t_mse)))
    plt.xticks(index, labels)
    plt.legend()

    plt.tight_layout()
    plt.show()


def _make_title(result):
    title = '\n'.join([
        f'hidden layers: {result.hidden_layer}',
        f'batch size: {result.batch_size}',
        f'activation: {result.activation}',
    ])
    return title


def show_sample(sample, result=None):
    sample = sample + sample.min()
    factor = 255 / sample.max()
    output = sample * factor

    if result:
        title = _make_title(result)
        plt.title(title)
    plt.imshow(output.reshape((28, 28)), cmap='gray')
    plt.show()


def visualize_run(ann, sample, result):
    _plot_mse(result)
    ff_matrix = ann.feed_forward(sample, w_matrix_list=result.weights)
    sample = ff_matrix[-1]
    show_sample(sample, result)


@q_wrapper
def q_1_1(result_list, sample):
    for i in [[32, ], [64, ], [128, ]]:
        ann = AutoEncoder(activation=0, hidden_layers=i, alpha=0.01)
        result = ann.fit('train_6.csv')
        result_list.append(result)
        visualize_run(ann, sample, result)
    print("hello, world!")


@q_wrapper
def q_1_2(result_list, sample):
    for i in [[256, 64, 256], [256, 128, 64, 128, 256]]:
        ann = AutoEncoder(activation=0, hidden_layers=i, alpha=0.01)
        result = ann.fit('train_6.csv')
        result_list.append(result)
        visualize_run(ann, sample, result)
    print("hello, world!")


@q_wrapper
def q_1_3(result_list, sample):
    for i in [0, 1, 2]:
        ann = AutoEncoder(activation=i, hidden_layers=[64], alpha=0.01)
        result = ann.fit('train_6.csv')
        result_list.append(result)
        visualize_run(ann, sample, result)
    print("hello, world!")


@q_wrapper
def q_1_4(result_list, sample):
    for i in [1, 0.5, 0.01, 0.001]:
        ann = AutoEncoder(activation=0, hidden_layers=[64], alpha=i)
        result = ann.fit('train_6.csv')
        result_list.append(result)
        visualize_run(ann, sample, result)
    print("hello, world!")


@q_wrapper
def q_1_5(result_list, sample):
    for i in [0, 0.1, .3, 0.5, 0.7]:
        ann = AutoEncoder(activation=0, hidden_layers=[64], alpha=0.01, miu=i)
        result = ann.fit('train_6.csv')
        result_list.append(result)
        visualize_run(ann, sample, result)
    print("hello, world!")


@q_wrapper
def q_1_6(result_list, sample):
    for i in [1, 8, 16]:
        ann = AutoEncoder(activation=0, hidden_layers=[64], alpha=0.01, batch_size=i)
        result = ann.fit('train_6.csv')
        result_list.append(result)
        visualize_run(ann, sample, result)
    print("hello, world!")


@q_wrapper
def q_2_0(result_list, sample):
    ann = AutoEncoder(activation=0, hidden_layers=[128], alpha=0.001)
    result = ann.fit('train_6.csv')
    result_list.append(result)
    visualize_run(ann, sample, result)
    print(result.out_dir)


def q_2():
    dir_name = 'MultiLayerPerceptron-20191122113818'
    ann = AutoEncoder(activation=0, hidden_layers=[128], alpha=0.001)
    classifier = AutoEncoderClassifier(ann, dir_name)
    data_6 = MultiLayerPerceptron.read_data('train_6.csv')
    data_non_6 = MultiLayerPerceptron.read_data('train_non_6.csv')
    data = np.concatenate((data_6, data_non_6), axis=0)
    label = np.zeros((data.shape[0],))
    label[data_6.shape[0]:] = 1
    # part 2, 3
    classifier.fit(data, label)
    # part 4
    data_6 = MultiLayerPerceptron.read_data('test_6.csv')
    data_non_6 = MultiLayerPerceptron.read_data('test_non_6.csv')
    data = np.concatenate((data_6, data_non_6), axis=0)
    label = np.zeros((data.shape[0],))
    label[data_6.shape[0]:] = 1
    predict_array = classifier.classify(data, crisp=True)
    classifier.make_result(label, predict_array, verbose=True)
    # part 5, 6
    raw_predict_array = classifier.classify(data)
    raw_positive = raw_predict_array[:data_6.shape[0]]
    raw_negative = raw_predict_array[data_6.shape[0]:]
    for j in range(1, 4):
        idx = raw_positive.argsort()[-j]
        print(predict_array[idx], raw_positive[idx])
        sample = data_6[idx]
        show_sample(sample)
    for j in range(0, 3):
        idx = raw_negative.argsort()[j]
        print(predict_array[data_6.shape[0] + idx], raw_negative[idx])
        sample = data_non_6[idx]
        show_sample(sample)


def test():
    print("hello, world!")


if __name__ == '__main__':
    # test()
    q_1_1()
