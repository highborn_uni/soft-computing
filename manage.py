import inspect
import sys
from datetime import datetime
from enum import Enum

from src.ex_1.adaline import Adaline
from src.ex_1.heb import HebNN
from src.ex_1.perceptron import Perceptron

__author__ = 'e.sadeqi.n'


class ProcessStatusType(Enum):
    started = 1
    finished = 2


def process_status_printer(status: ProcessStatusType):
    time_format = '%Y-%m-%d %H:%M:%S'
    current_frame = inspect.currentframe()
    outer_frame = inspect.getouterframes(current_frame, 2)
    function_name = outer_frame[1][3]
    current_time = datetime.now().strftime(time_format)
    print(current_time, function_name, status.name, sep='\t')


def heb():
    process_status_printer(ProcessStatusType.started)

    ann = HebNN()
    ann.fit()

    process_status_printer(ProcessStatusType.finished)


def perceptron(*args):
    process_status_printer(ProcessStatusType.started)

    alpha, gama, theta = _ann_params(*args)
    ann = Perceptron(alpha=alpha, gama=gama, theta=theta)
    ann.fit()

    process_status_printer(ProcessStatusType.finished)


def adaline(*args):
    process_status_printer(ProcessStatusType.started)

    alpha, gama, theta = _ann_params(*args)
    ann = Adaline(alpha=alpha, gama=gama, theta=theta)
    ann.fit()

    process_status_printer(ProcessStatusType.finished)


def _ann_params(*args):
    help_msg = (
        """
                available commands are: [-a float][-g float][-t float][-h][]

                '-a' :\talpha ratio.
                '-g' :\tgama, the bound of random weight generation.
                '-h' :\thelp message.
                '-t' :\ttheta, threshold of activation function.

        """
    )

    alpha = None
    gama = None
    theta = None
    for i in range(len(args)):
        if args[i] == '-h':
            print(help_msg)
            return
        elif args[i] == '-a':
            alpha = float(args[i + 1])
        elif args[i] == '-g':
            gama = float(args[i + 1])
        elif args[i] == '-t':
            theta = float(args[i + 1])
    return alpha, gama, theta


if __name__ == '__main__':
    argv = sys.argv
    available_function_list = [
        heb,
        perceptron,
        adaline,
    ]
    function_names = ""
    for function_ in available_function_list:
        function_names += "\t{}\n".format(function_.__name__)
    man_msg = (
        "Possible commands are [\n{command_names}]\n\n"
        "Special thanks to {author}, the author of program :D"
    ).format(
        command_names=function_names,
        author=__author__
    )

    if len(argv) < 2:
        print("Empty command")
        print(man_msg)
        exit(-1)

    _, command, *arguments = argv
    command = str(command).strip()

    for function_ in available_function_list:
        if command == function_.__name__:
            function_(*arguments)
            exit(0)
    else:
        print("Wrong command:\t" + command)
        print(man_msg)
